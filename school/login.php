<?php
session_start();
require_once "validate.php";
require_once "dbconnect.php";
require_once "dbconfig.php";
require_once "users.php";


$errors = array();

if (isset($errors)) {
    $errors = valid($errors);
}

$users    = new users($dbConnection);
$email    = $_POST['email'];
$password = $_POST['password'];

if (isValid($errors)) {
    if (isset($_POST['login'])) {
        $logged_user = array();
        $logged_user = $users->getUser($email, $password);

        if ($logged_user == "null") {
            $_SESSION['message'] = "Wrong email or password";
        } else {
            $_SESSION['id']     = $logged_user['user_id'];
            $_SESSION['name']   = $logged_user['name'];
            $_SESSION['email']  = $logged_user['email'];
            $_SESSION['role']   = $logged_user['role'];
            $_SESSION['status'] = $logged_user['status'];
            header('Location: index.php?confirmation=success');
        }
    }
}
?>
<html>

<body>
<?php
if (isset($_SESSION['message'])) {
    echo "<div id='error_msg'>" . $_SESSION['message'] . "</div>";
    unset($_SESSION['message']);
}
?>
<form method="post" action="">
    Username: <input type="email" name="email" value="<?php echo htmlspecialchars($email); ?>">
    <span class="error">* <?php echo $errors["emailErr"]; ?></span>
    <br><br>
    Password: <input type="password" name="password" value="<?php echo htmlspecialchars($password); ?>">
    <span class="error">* <?php echo $errors["passwordErr"]; ?></span>
    <br><br>
    <input type="submit" name="login" value="Login">
</form>

</body>
</html>
