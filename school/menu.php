<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .vertical-menu {
            width: 200px;
        }

        .vertical-menu a {
            background-color: #eee;
            color: black;
            display: block;
            padding: 12px;
            text-decoration: none;
        }

        .vertical-menu a:hover {
            background-color: #ccc;
        }

        .vertical-menu a.active {
            background-color: #4CAF50;
            color: white;
        }
    </style>

</head>
<body>


<div class="vertical-menu">
    <a href="showCategories.php" name="categories">Categories</a>
    <a href="showSubcategories.php" name="subcategories">Subcategories</a>
    <a href="showSubjects.php" name="subjects">Subjects</a>
    <a href="showUsers.php" name="users">Users</a>
</div>

</body>
</html>
<?php


?>