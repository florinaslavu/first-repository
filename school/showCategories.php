<?php
session_start();

if (isset($_SESSION['id'])) {
    require_once "dbconnect.php";
    require_once "dbconfig.php";
    require_once "users.php";
    if ($_SESSION['role'] == 0) {
        require_once "menu.php";
    } else {
        require_once "menuProfessor.php";
    }

    $users = new users($dbConnection);


    $categories = $users->getAllCategories();
    $users->showAllCategories($categories);


} else {
    $_SESSION['message'] = "You are not logged.";
}

if (isset($_SESSION['message'])) {
    echo "<div id='error_msg'>" . $_SESSION['message'] . "</div>";
    unset($_SESSION['message']);
}

?>

<html>
<body>
</form> <br><br>
<button class="button"><a href="index.php">Main Menu</a></button>
</body>
</html><br><br>
<form method="post" action="addNewCategory.php">
    <input type="submit" name="addnewcategory" value="Add New Category">
</form>
</body>
</html>

<form method="post" action="logout.php">
    <input type="submit" name="logout" value="Logout">
</form>
