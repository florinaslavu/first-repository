<?php
session_start();

if (isset($_SESSION['id'])) {

    require_once "dbconnect.php";
    require_once "dbconfig.php";
    require_once "users.php";
    if ($_SESSION['role'] == 0) {
        require_once "menu.php";
    } else {
        require_once "menuProfessor.php";
    }

    $users        = new users($dbConnection);
    $subject_name = $_POST['subject_name'];

    if (isset($_POST['upload'])) {

        $_SESSION['message']= $users->uploadFile();
    }

} else {
    $_SESSION['message'] = "You are not logged.";
}

if (isset($_SESSION['message'])) {
    echo "<div id='error_msg'>" . $_SESSION['message'] . "</div>";
    unset($_SESSION['message']);
}

?>

<html>
<br><br>
<head>
    <script>
        function reload(form) {
            var val = form.dropdowncategories.options[form.dropdowncategories.options.selectedIndex].value;
            self.location = 'addNewSubject.php?id_category=' + val;
        }

    </script>
</head>

<body>
<form action="" method="post" enctype="multipart/form-data">
    Category name: <select name="dropdowncategories" onchange="reload(this.form)">
        <option value=""><?php echo $_GET['id_category']; ?></option>
        <?php $users->dropDownCategories(); ?>   </select>*
    <br><br>
    Subcategory name: <select name="dropdownsubcategories">
        <option value=""></option>
        <?php $users->selectDropDown(); ?>   </select>*
    <br><br>
    Subject name: <input type="text" name="subject_name">
    <span class="error">* </span>
    <br><br>
    Select File to Upload:
    <input type="file" name="file">
    <input type="submit" name="upload" value="Upload">
</form>

<button class="button"><a href="showSubjects.php">Main Menu</a></button>
<br>
</body>
</html>
