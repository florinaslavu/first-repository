<?php
require_once "validate.php";

class employees
{
    private $id;
    private $firstname;
    private $lastname;
    private $adress;
    private $department;
    private $dbconfig;

    function __construct($dbconfig)
    {
        $this->dbconfig = $dbconfig;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function setAdress($adress)
    {
        $this->adress = $adress;
    }

    public function setDepartment($department)
    {
        $this->department = $department;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function getAdress()
    {
        return $this->adress;
    }

    public function getDepartment()
    {
        return $this->department;
    }

    function executeQuery($sql)
    {
        return mysqli_query($this->dbconfig, $sql);
    }

    function getAllEmployees()
    {
        $staff  = [];
        $sql    = "SELECT * FROM dbname.Angajati";
        $result = $this->executeQuery($sql);

        if (mysqli_num_rows($result) > 0) {
            While ($row = $result->fetch_assoc()) {
                $staff[$row['id']] = $row;
            }
        }

        return $staff;
    }

    function viewEmployees($staff)
    {
        echo "<table>";

        foreach ($staff as $key => $info) {
            print("<tr>\n");
            print("<td>" . $info['id'] . "</td>\n");
            print("<td>" . $info['firstname'] . "</td>\n");
            print("<td>" . $info['lastname'] . "</td>\n");
            print("<td>" . $info['adress'] . "</td>\n");
            print("<td>" . $info['department'] . "</td>\n");
            print("<td><a href='delete.php?comanda=delete&id=" . $info['id'] . "'>Delete</a></td>\n");
            print("<td><a href='edit.php?comanda=edit&id=" . $info['id'] . "'>Edit</a></td>\n");
            print("</tr>\n");
        }
        echo "</table>";
    }

    function delete()
    {
        $id  = $_GET['id'];
        $sql = "DELETE FROM dbname.Angajati WHERE id='$id'";

        if (!$this->executeQuery($sql)) {
            die('Error: ' . mysqli_error($this->dbconfig));
        } else {
            header('Location: index.php?confirmation=success');
        }
    }

    function add($errors)
    {
        $firstname  = $_REQUEST["firstname"];
        $lastname   = $_REQUEST["lastname"];
        $adress     = $_REQUEST["adress"];
        $department = $_REQUEST["department"];

        if (isValid($errors)) {
            $check = "SELECT * FROM dbname.Angajati WHERE nume='$firstname'";

            if (mysqli_num_rows($this->executeQuery($check)) >= 1) {
                echo "User Already  Exists<br/>";
                $firstname = $lastname = $adress = $department = "";
            } else {
                $sql = "INSERT INTO dbname.Angajati(nume, prenume, adresa, departament) 
                       VALUES ('$firstname','$lastname', '$adress', '$department')";

                if ($this->executeQuery($sql)) {
                    echo "Employee added";
                } else {
                    echo "Employee not added";
                }
            }
        }
    }

    function getInfo($employeeInformations = array())
    {
        $id  = $_GET['id'];
        $sql = "SELECT * FROM dbname.Angajati WHERE id='$id'";

        $result = $this->executeQuery($sql);

        if ($result) {
            While ($row = $result->fetch_assoc()) {
                $employeeInformations["id"]         = $row['id'];
                $employeeInformations["firstname"]  = $row['firstname'];
                $employeeInformations["lastname"]   = $row['lastname'];
                $employeeInformations["adress"]     = $row['adress'];
                $employeeInformations["department"] = $row['department'];
            }
        }

        return $employeeInformations;
    }

    function update($erori)
    {
        $employeeInformations["firstname"]  = $_POST["firstname"];
        $employeeInformations["lastname"]   = $_POST["lastname"];
        $employeeInformations["adress"]     = $_POST["adress"];
        $employeeInformations["department"] = $_POST["department"];

        $id = $_GET['id'];

        if (isset($_POST['edit'])) {
            if (isValid($erori)) {
                $sql = "UPDATE dbname.Angajati
                                 SET  nume='" . $employeeInformations["firstname"] . "',prenume='" . $employeeInformations["lastname"] . "',
                                  adresa='" . $employeeInformations["adress"] . "', departament='" . $employeeInformations["department"] . "'
                                  WHERE id= '$id'";

                if ($this->executeQuery($sql)) {
                    echo "Updated";
                } else {
                    echo "Not updated";
                }
            }
        }
    }
}

?>