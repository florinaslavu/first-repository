<?php

function validUser($errors = array())
{
    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if (empty($_POST["name"])) {
            $errors["nameErr"] = "Required";
        }
        if (empty($_POST["email"])) {
            $errors["emailErr"] = "Required";
        }

        if (empty($_POST["password"])) {
            $errors["passwordErr"] = "Required";
        }

        if (empty($_POST["role"]) && $_POST["role"]!=0) {
            $errors["roleErr"] = "Required";
        }
        if (empty($_POST["status"]) && $_POST["status"]!=0) {
            $errors["statusErr"] = "Required";
        }
    }
        return $errors;

}

function isValidUser($errors)
{
    $errors = validUser($errors);
    if (empty($errors)) {
        return true;
    }

    return false;
}

?>