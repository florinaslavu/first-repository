<?php

function valid($errors = array())
{
    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if (empty($_POST["email"])) {
            $errors["emailErr"] = "Required";
        }
        if (empty($_POST["password"])) {
            $errors["passwordErr"] = "Required";
        }

        return $errors;
    }
}

function isValid($errors)
{
    $errors = valid($errors);
    if (empty($errors)) {
        return true;
    }

    return false;
}

?>