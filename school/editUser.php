<?php
session_start();
if (isset($_SESSION['id'])) {
    require_once "dbconnect.php";
    require_once "dbconfig.php";
    require_once "users.php";
    if ($_SESSION['role'] == 0) {
        require_once "menu.php";
    } else {
        require_once "menuProfessor.php";
    }
    require_once "validateNewUser.php";
  //  require_once "showUsers.php";



    $users    = new users($dbConnection);
    $allUsers = $users->getAllUsers();

    $userInformations = $users->getInfo($userInformations);

    $name     = $userInformations["name"];
    $email    = $userInformations["email"];
    $password = $userInformations["password"];
    $role     = $userInformations["role"];
    $status   = $userInformations["status"];

    $errors = array();

    if (isset($errors)) {
        $errors = validUser($errors);
    }
    $users->update($errors);

} else {
    $_SESSION['message'] = "You are not logged.";
}

if (isset($_SESSION['message'])) {
    echo "<div id='error_msg'>" . $_SESSION['message'] . "</div>";
    unset($_SESSION['message']);
}
?>

<form method="post" action="">
    <p><span class="error">* required field</span></p>
    Name: <input type="text" name="name" value="<?php echo $name; ?>">
    <span class="error">*  <?php echo $errors["nameErr"]; ?></span>
    <br><br>
    Email: <input type="email" name="email" value="<?php echo $email; ?>">
    <span class="error">*  <?php echo $errors["emailErr"]; ?></span>
    <br><br>
    Password: <input type="password" name="password" value="<?php echo $password; ?>">
    <span class="error">*  <?php echo $errors["passwordErr"]; ?></span>
    <br><br>
    Role: <input type="text" name="role" value="<?php echo $role; ?>">
    <span class="error">*  <?php echo $errors["roleErr"]; ?></span>
    <br><br>
    Status: <input type="text" name="status" value="<?php echo $status; ?>">
    <span class="error">* <?php echo $errors["statusErr"]; ?></span>
    <input type="submit" name="edit" value="Update">
</form>

<br><br>

<html>
<body>

<button class="button"><a href="showUsers.php">Back</a></button>
<br>
</body>
</html>
