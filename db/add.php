<html>
<body>
<?php
require_once "dbconfig.php";
require_once "validate.php";
require_once "employees.php";

$conn         = new dbconfig("localhost", "root", "qwqwqwqw", "dbname");
$dbConnection = $conn->getConnection();

if (!$dbConnection) {
    die("Connection failed");
}
$employees = new employees($dbConnection);
$staff     = $employees->getAllEmployees();
$errors    = array();

if (isset($errors)) {
    $errors = valid($errors);
}

$employees->add($errors);
?>

<p><span class="error">* required field</span></p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    Name: <input type="text" name="firstname" value="<?php echo htmlspecialchars($firstname); ?>">
    <span class="error">* <?php echo $errors["firstnameErr"]; ?></span>
    <br><br>
    Prenume: <input type="text" name="lastname" value="<?php echo htmlspecialchars($lastname); ?>">
    <span class="error">* <?php echo $errors["lastnameErr"]; ?></span>
    <br><br>
    Adresa: <input type="text" name="adress" value="<?php echo htmlspecialchars($adress); ?>">
    <span class="error">* <?php echo $errors["adressErr"]; ?></span>
    <br><br>
    Departament: <input type="text" name="department" value="<?php echo htmlspecialchars($department); ?>">
    <span class="error">* <?php echo $errors["departmentErr"]; ?></span>
    <br><br>
    <input type="submit" name="submit" value="Submit">
</form>
<br><br>

<button class="button"><a href="index.php">Main Menu</a></button>
<br>

</body>
</html>