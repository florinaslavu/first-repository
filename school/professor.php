<?php
session_start();

if (isset($_SESSION['id'])) {

    if ($_SESSION['status'] == 1) {

        require_once "dbconnect.php";
        require_once "dbconfig.php";
        require_once "users.php";
        require_once "menuProfessor.php";


        $users = new users($dbConnection);
    } else {
        $_SESSION['message'] = "You have no permision";
    }

} else {
    $_SESSION['message'] = "You are not logged.";
}

if (isset($_SESSION['message'])) {
    echo "<div id='error_msg'>" . $_SESSION['message'] . "</div>";
    unset($_SESSION['message']);
}

?>

<form method="post" action="logout.php">
    <input type="submit" name="logout" value="Logout">
</form>
