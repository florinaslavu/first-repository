<?php
require_once "dbconfig.php";
require_once "employees.php";

$conn         = new dbconfig("localhost", "root", "qwqwqwqw", "dbname");
$dbConnection = $conn->getConnection();

if (!$dbConnection) {
    die("Connection failed");
}
$employees = new employees($dbConnection);
$staff  = $employees->getAllEmployees();
$employees->viewEmployees($staff);

$errors = [];
if (isset($_POST['add'])) {
    $employees->add($errors);
}
?>

<html>
<body>
<br><br>
<form method="post" action="add.php">
    <input type="submit" name="add" value="Add New Employee">
</form>
</body>
</html>
