<?php
session_start();
if (isset($_SESSION['id'])) {
    require_once "dbconnect.php";
    require_once "dbconfig.php";
    require_once "users.php";
    require_once "menu.php";
    require_once "validateNewUser.php";

    $users  = new users($dbConnection);
    $errors = array();

    if (isset($errors)) {
        $errors = validUser($errors);
    }
    $users->addNewUser($errors);

} else {
    $_SESSION['message'] = "You are not logged.";
}

if (isset($_SESSION['message'])) {
    echo "<div id='error_msg'>" . $_SESSION['message'] . "</div>";
    unset($_SESSION['message']);
}

?>

<form method="post" action="">
    <p><span class="error">* required field</span></p>
    Name: <input type="text" name="name">
    <span class="error">*  <?php echo $errors["nameErr"]; ?></span>
    <br><br>
    Email: <input type="email" name="email">
    <span class="error">*  <?php echo $errors["emailErr"]; ?></span>
    <br><br>
    Password: <input type="password" name="password">
    <span class="error">*  <?php echo $errors["passwordErr"]; ?></span>
    <br><br>
    Role: <input type="text" name="role" >
    <span class="error">*  <?php echo $errors["roleErr"]; ?></span>
    <br><br>
    Status: <input type="text" name="status" >
    <span class="error">* <?php echo $errors["statusErr"]; ?></span>
    <input type="submit" name="addNewUser" value="Add">
</form>
<br><br>

<html>
<body>

<button class="button"><a href="showUsers.php">Back</a></button>
<br>
</body>
</html>