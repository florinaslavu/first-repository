<?php

session_start();

if (isset($_SESSION['id'])) {
    require_once "dbconnect.php";
    require_once "dbconfig.php";
    require_once "users.php";
    if ($_SESSION['role'] == 0) {
        require_once "menu.php";
    } else {
        require_once "menuProfessor.php";
    }

    $users  = new users($dbConnection);
    $errors = [];

    if (isset($_POST['add'])) {
        if (empty($_POST["category_name"])) {
            $errors["category_nameErr"] = "Required";
        } else {
            $users->addNewCategory();
        }
    }

} else {
    $_SESSION['message'] = "You are not logged.";
}

if (isset($_SESSION['message'])) {
    echo "<div id='error_msg'>" . $_SESSION['message'] . "</div>";
    unset($_SESSION['message']);
}

?>
<html>

<p><span class="error">* required field</span></p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    Category Name: <input type="text" name="category_name" value="<?php echo htmlspecialchars($category_name); ?>">
    <span class="error">* <?php echo $errors["category_nameErr"]; ?></span>
    <br><br>
    <input type="submit" name="add" value="Add">
</form>


<button class="button"><a href="showCategories.php">Back</a></button>
<br>

</form>
</html>