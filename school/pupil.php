<?php
session_start();

if (isset($_SESSION['id'])) {

    require_once "dbconnect.php";
    require_once "dbconfig.php";
    require_once "users.php";


    $users = new users($dbConnection);

    if ($_SESSION['status'] == 1) {

        $subjects = $users->getAllSubjects();
        $users->showAllSubjects($subjects);

    } else {
        $_SESSION['message'] = "You have no permission.";
    }
} else {
    $_SESSION['message'] = "You are not logged.";
}

if (isset($_SESSION['message'])) {
    echo "<div id='error_msg'>" . $_SESSION['message'] . "</div>";
    unset($_SESSION['message']);
}
?>

<form method="post" action="logout.php">
    <input type="submit" name="logout" value="Logout">
</form>
