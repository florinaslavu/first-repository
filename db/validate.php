<?php

function valid($errors = array())
{
    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if (empty($_POST["firstname"])) {
            $errors["firstnameErr"] = "Required";
        }
        if (empty($_POST["lastname"])) {
            $errors["lastnameErr"] = "Required";
        }

        if (empty($_POST["adress"])) {
            $errors["adressErr"] = "Required";
        }

        if (empty($_POST["department"])) {
            $errors["departmentErr"] = "Required";
        }

        return $errors;
    }
}

function isValid($errors)
{
    $errors = valid($errors);
    if (empty($errors)) {
        return true;
    }

    return false;
}

?>