<?php
session_start();
if (isset($_SESSION['id'])) {
    require_once "dbconnect.php";
    require_once "dbconfig.php";
    require_once "users.php";
    require_once "menu.php";

    $users = new users($dbConnection);

    $users->delete();

} else {
    $_SESSION['message'] = "You are not logged.";
}

if (isset($_SESSION['message'])) {
    echo "<div id='error_msg'>" . $_SESSION['message'] . "</div>";
    unset($_SESSION['message']);
}


?>

<html>
<body>

<button class="button"><a href="showUsers.php">Back</a></button>
<br>
</body>
</html>
