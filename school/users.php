<?php
session_start();
require_once "validateNewUser.php";

class users
{
    private $id;
    private $name;
    private $email;
    private $role;
    private $status;
    private $password;

    function __construct($dbconnect)
    {
        $this->dbconnect = $dbconnect;
    }

    function executeQuery($sql)
    {
        return mysqli_query($this->dbconnect, $sql);
    }

    function getUser($email, $password)
    {
        $logged_user = [];
        $sql         = "SELECT * FROM school.users WHERE email='$email' AND password='$password'";
        $result      = $this->executeQuery($sql);
        if (mysqli_num_rows($result) > 0) {
            While ($row = $result->fetch_assoc()) {
                $logged_user = $row;
            }
        } else {
            $logged_user = "null";
        }

        return $logged_user;
    }

    function getAllCategories()
    {

        $categories = [];
        $sql        = "SELECT * FROM school.categories";
        $result     = $this->executeQuery($sql);
        if (mysqli_num_rows($result) > 0) {
            While ($row = $result->fetch_assoc()) {

                $categories[$row['id_category']] = $row;

            }
        }

        return $categories;
    }


    function showAllCategories($categories)
    {
        echo "<table>";

        foreach ($categories as $key => $info) {
            print("<tr>\n");
            print("<td>" . $info['id_category'] . "</td>\n");
            print("<td>" . $info['category_name'] . "</td>\n");
            print("<td><a href='deleteCategory.php?comanda=delete&id_category=" . $info['id_category'] . "'>Delete</a></td>\n");
            print("</tr>\n");
        }
        echo "</table>";
    }

    function getAllSubcategories()
    {
        $subcategories = [];
        $sql           = "SELECT categories.id_category, categories.category_name,subcategories.subcategory_name, subcategories.id_subcategory 
                            FROM  school.categories
                             INNER  JOIN school.subcategories ON categories.id_category=subcategories.id_category";
        $result        = $this->executeQuery($sql);

        While ($row = $result->fetch_assoc()) {
            $subcategories[$row['id_subcategory']] = $row;
        }

        return $subcategories;
    }

    function showAllSubcategories($subcategories)
    {
        echo "<table>";

        foreach ($subcategories as $key => $info) {
            print("<tr>\n");
            print("<td>" . $info['id_subcategory'] . "</td>\n");
            print("<td>" . $info['category_name'] . "</td>\n");
            print("<td>" . $info['subcategory_name'] . "</td>\n");
            print("<td><a href='deleteSubcategory.php?comanda=delete&id_subcategory=" . $info['id_subcategory'] . "'>Delete</a></td>\n");
            print("</tr>\n");
        }
        echo "</table>";
    }

    function addNewCategory()
    {
        $category_name = $_REQUEST["category_name"];

        $check = "SELECT * FROM school.categories WHERE category_name='$category_name'";

        if (mysqli_num_rows($this->executeQuery($check)) >= 1) {
            echo "Category Already  Exists<br/>";

        } else {
            $sql = "INSERT INTO `school`.`categories` (`category_name`) 
                       VALUES ('$category_name')";

            if ($this->executeQuery($sql)) {
                echo "Category added";
            } else {
                echo "Category not added";
            }
        }

    }

    function dropDownCategories()
    {
        $sql    = "SELECT * FROM school.categories";
        $result = $this->executeQuery($sql);
        if (mysqli_num_rows($result) > 0) {
            While ($row = $result->fetch_assoc()) {
                $id_category   = $row['id_category'];
                $category_name = $row['category_name'];
                echo '<option value="' . $id_category . '">' . $category_name . '</option>';
            }
        }
    }

    function dropDownSubcategories()
    {
        $sql    = "SELECT categories.id_category, categories.category_name,subcategories.subcategory_name 
                            FROM  school.categories
                             INNER  JOIN school.subcategories ON categories.id_category=subcategories.id_category";
        $result = $this->executeQuery($sql);
        if ($result) {
            While ($row = $result->fetch_assoc()) {
                $id_subcategory   = $row['id_subcategory'];
                $subcategory_name = $row['subcategory_name'];
                echo '<option value="' . $id_subcategory . '">' . $subcategory_name . '</option>';
            }
        }
    }

    function addNewSubcategory()
    {
        $subcategory_name = $_POST["subcategory_name"];
        $id_category      = $_POST['dropdowncategories'];
        $sql              = "INSERT INTO school.subcategories (id_category,subcategory_name) 
                       VALUES ('$id_category','$subcategory_name')";

        if ($this->executeQuery($sql)) {
            echo "Subcategory added";
        } else {
            echo "Subcategory not added";
        }
    }


    function getAllUsers()
    {
        $allUsers = [];
        $sql      = "SELECT * FROM school.users";
        $result   = $this->executeQuery($sql);

        if (mysqli_num_rows($result) > 0) {
            While ($row = $result->fetch_assoc()) {
                $allUsers[$row['user_id']] = $row;
            }
        }

        return $allUsers;
    }

    function viewAllUsers($allUsers)
    {
        echo "<table>";

        foreach ($allUsers as $key => $info) {
            print("<tr>\n");
            print("<td>" . $info['user_id'] . "</td>\n");
            print("<td>" . $info['name'] . "</td>\n");
            print("<td>" . $info['email'] . "</td>\n");
            if ($info['role'] == 0) {
                print("<td>" . "admin" . "</td>\n");
            }
            if ($info['role'] == 1) {
                print("<td>" . "professor" . "</td>\n");
            }
            if ($info['role'] == 2) {
                print("<td>" . "pupil" . "</td>\n");
            }
            if ($info['status'] == 0) {
                print("<td>" . "inactive" . "</td>\n");
            }
            if ($info['status'] == 1) {
                print("<td>" . "active" . "</td>\n");
            }
            print("<td><a href='deleteUser.php?comanda=delete&user_id=" . $info['user_id'] . "'>Delete</a></td>\n");
            print("<td><a href='editUser.php?comanda=edit&user_id=" . $info['user_id'] . "'>Edit</a></td>\n");
            print("</tr>\n");
        }
        echo "</table>";
    }

    function delete()
    {
        $user_id = $_GET['user_id'];
        $sql     = "DELETE FROM school.users WHERE user_id='$user_id'";

        if (!$this->executeQuery($sql)) {
            die('Error: ' . mysqli_error($this->dbconnect));
        } else {
            echo "User deleted";
        }
    }

    function deleteCategory()
    {
        $id_category = $_GET['id_category'];
        $sql         = "DELETE FROM school.categories WHERE id_category='$id_category'";

        if (!$this->executeQuery($sql)) {
            die('Error: ' . mysqli_error($this->dbconnect));
        } else {
            echo "Category deleted";
        }
    }

    function deleteSubcategory()
    {
        $id_subcategory = $_GET['id_subcategory'];
        $sql            = "DELETE FROM school.subcategories WHERE id_subcategory='$id_subcategory'";

        if (!$this->executeQuery($sql)) {
            die('Error: ' . mysqli_error($this->dbconnect));
        } else {
            echo "Subcategory deleted";
        }
    }

    function getInfo($userInformations = array())
    {
        $user_id = $_GET['user_id'];
        $sql     = "SELECT * FROM school.users WHERE user_id='$user_id'";

        $result = $this->executeQuery($sql);

        if ($result) {
            While ($row = $result->fetch_assoc()) {
                $userInformations["user_id"]  = $row['user_id'];
                $userInformations["name"]     = $row['name'];
                $userInformations["email"]    = $row['email'];
                $userInformations["password"] = $row['password'];
                $userInformations["role"]     = $row['role'];
                $userInformations["status"]   = $row['status'];
            }
        }

        return $userInformations;
    }

    function update($errors)
    {
        $userInformations["name"]     = $_POST["name"];
        $userInformations["email"]    = $_POST["email"];
        $userInformations["password"] = $_POST["password"];
        $userInformations["role"]     = $_POST["role"];
        $userInformations["status"]   = $_POST["status"];

        $user_id = $_GET['user_id'];

        if (isset($_POST['edit'])) {
            if (isValidUser($errors)) {
                $sql = "UPDATE school.users
                        SET  name='" . $userInformations["name"] . "',email='" . $userInformations["email"] . "',
                                     password='" . $userInformations["password"] . "',
                                     role='" . $userInformations["role"] . "', status='" . $userInformations["status"] . "'
                        WHERE user_id= '$user_id'";

                if ($this->executeQuery($sql)) {
                    echo "Updated";
                } else {
                    echo "Not updated";
                }
            }
        }
    }

    function addNewUser($errors)
    {
        $name     = $_REQUEST["name"];
        $email    = $_REQUEST["email"];
        $password = $_REQUEST["password"];
        $role     = $_REQUEST["role"];
        $status   = $_REQUEST["status"];

        if (isValidUser($errors)) {
            $check = "SELECT * FROM school.users WHERE name='$name'";

            if (mysqli_num_rows($this->executeQuery($check)) >= 1) {
                echo "User Already  Exists<br/>";
            } else {
                $sql = "INSERT INTO school.users(name, email, password, role, status) 
                       VALUES ('$name','$email', '$password', '$role','$status')";

                if ($this->executeQuery($sql)) {
                    echo "User added";
                } else {
                    echo "User not added";
                }
            }
        }
    }

    function getAllSubjects()
    {
        $subjects = [];
        $sql      = "SELECT categories.id_category,categories.category_name,subjects.id_category,subjects.id_subcategory,
                              subcategories.id_subcategory,
                            subcategories.subcategory_name, subjects.subject_name,subjects.file_name,subjects.id                            
                  FROM school.subjects 
                      INNER JOIN  school.categories ON categories.id_category=subjects.id_category
                        INNER JOIN school.subcategories ON subcategories.id_subcategory=subjects.id_subcategory";
        $result   = $this->executeQuery($sql);

        While ($row = $result->fetch_assoc()) {

            $subjects[$row['id']] = $row;

        }

        return $subjects;
    }

    function showAllSubjects($subjects)
    {
        echo "<table>";

        foreach ($subjects as $key => $info) {
            print("<tr>\n");
            print("<td>" . $info['id'] . "</td>\n");
            print("<td>" . $info['category_name'] . "</td>\n");
            print("<td>" . $info['subcategory_name'] . "</td>\n");
            print("<td>" . $info['subject_name'] . "</td>\n");
            print("<td>" . $info['file_name'] . "</td>\n");
            print("<td><a href='downloadSubject.php?comanda=download&id=" . $info['id'] . "'>Download</a></td>\n");
            print("<td><a href='deleteSubject.php?comanda=delete&id=" . $info['id'] . "'>Delete</a></td>\n");
            print("</tr>\n");
        }
        echo "</table>";

    }

    function deleteSubject()
    {
        $id  = $_GET['id'];
        $sql = "DELETE FROM school.subjects WHERE id='$id'";

        if (!$this->executeQuery($sql)) {
            die('Error: ' . mysqli_error($this->dbconnect));
        } else {
            echo "Subject deleted";
        }
    }

    function uploadFile()
    {
        $targetDir      = "uploads/";
        $fileName       = basename($_FILES['file']['name']);
        $targetFilePath = $targetDir . $fileName;
        $subject_name   = $_POST['subject_name'];
        $user_id        = $_SESSION['id'];
        $id_category    = $_GET['id_category'];
        $id_subcategory = $_POST['dropdownsubcategories'];


        if (move_uploaded_file($_FILES['file']['tmp_name'], $targetFilePath)) {

            $sql = "INSERT INTO school.subjects (id_category,id_subcategory,subject_name,file_name,user_id) 
                      VALUES ('$id_category','$id_subcategory','$subject_name','$fileName','$user_id')";


            if ($this->executeQuery($sql)) {
                $statusMsg = "The file  has been uploaded successfully.";
            } else {
                $statusMsg = "File upload failed, please try again.";
            }
        } else {
            $statusMsg = "Sorry, there was an error uploading your file.";
        }


        return $statusMsg;
    }

    function downloadSubject()
    {
        $id     = $_GET['id'];
        $sql    = "SELECT * FROM school.subjects WHERE id='$id'";
        $result = $this->executeQuery($sql);
        While ($row = $result->fetch_assoc()) {
            $fileName = $row['file_name'];
            $path     = 'downloads/' . $fileName;
            $size     = filesize($path);
            header('Content-Type: application/octet-stream');
            header('Content-Length: ' . $size);
            header('Content-Disposition: attachment; filename=' . $fileName);
            header('Content-Transfer-Encoding: binary');

        }
    }

    function selectDropDown()
    {

        @$id_category = $_GET['id_category'];
        $sql    = "SELECT DISTINCT id_subcategory,id_subcategory,subcategory_name 
                    FROM school.subcategories 
                    WHERE id_category=$id_category";
        $result = $this->executeQuery($sql);
        if (mysqli_num_rows($result) > 0) {
            While ($row = $result->fetch_assoc()) {
                $id_subcategory   = $row['id_subcategory'];
                $subcategory_name = $row['subcategory_name'];
                echo '<option value="' . $id_subcategory . '">' . $subcategory_name . '</option>';
            }
        }

    }
}

?>