<html>
<body>
<?php
require_once "dbconfig.php";
require_once "validate.php";
require_once "employees.php";

$conn         = new dbconfig("localhost", "root", "qwqwqwqw", "dbname");
$dbConnection = $conn->getConnection();

if (!$dbConnection) {
    die("Connection failed");
}

$employees = new employees($dbConnection);
$staff     = $employees->getAllEmployees();

$employeeInformations = $employees->getInfo($employeeInformations);

$firstname  = $employeeInformations["firstname"];
$lastname   = $employeeInformations["lastname"];
$adress     = $employeeInformations["adress"];
$department = $employeeInformations["department"];

$errors = array();

if (isset($errors)) {
    $errors = valid($errors);
}

$employees->update($errors);
?>

<form method="post" action="">
    <p><span class="error">* required field</span></p>
    Name: <input type="text" name="firstname" value="<?php echo $firstname; ?>">
    <span class="error">*  <?php echo $errors["firstnameErr"]; ?></span>
    <br><br>
    Prenume: <input type="text" name="lastname" value="<?php echo $lastname; ?>">
    <span class="error">*  <?php echo $errors["lastnameErr"]; ?></span>
    <br><br>
    Adresa: <input type="text" name="adress" value="<?php echo $adress; ?>">
    <span class="error">*  <?php echo $errors["adressErr"]; ?></span>
    <br><br>
    Departament: <input type="text" name="department" value="<?php echo $department; ?>">
    <span class="error">* <?php echo $errors["departmentErr"]; ?></span>
    <input type="submit" name="edit" value="Update">
</form>

<br><br>

<button class="button"><a href="index.php">Main Menu</a></button>
<br>