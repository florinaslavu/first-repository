<?php

session_start();

if (isset($_SESSION['id'])) {
    require_once "dbconnect.php";
    require_once "dbconfig.php";
    require_once "users.php";
    if ($_SESSION['role'] == 0) {
        require_once "menu.php";
    } else {
        require_once "menuProfessor.php";
    }

    $users  = new users($dbConnection);
    $errors = [];

    if (isset($_POST['add'])) {

        if (empty($_POST["dropdowncategories"])) {
            $errors["dropdownmenuErr"] = "Required";
        }
        if (empty($_POST["subcategory_name"])) {
            $errors["subcategory_nameErr"] = "Required";
        }
        if (empty($errors)) {
            $users->addNewSubcategory();
        }
    }

} else {
    $_SESSION['message'] = "You are not logged.";
}

if (isset($_SESSION['message'])) {
    echo "<div id='error_msg'>" . $_SESSION['message'] . "</div>";
    unset($_SESSION['message']);
}

?>

<p><span class="error">* required field</span></p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    Category name: <select name="dropdowncategories">
        <option value=""></option>
        <?php $users->dropDownCategories(); ?>   </select>*<?php echo $errors["dropdownmenuErr"]; ?>
    <br><br>
    Subcategory Name: <input type="text" name="subcategory_name">
    <span class="error">* <?php echo $errors["subcategory_nameErr"]; ?></span>
    <br><br>
    <input type="submit" name="add" value="Add">
</form>


<button class="button"><a href="showSubcategories.php">Back</a></button>
<br>

</form>
</html>