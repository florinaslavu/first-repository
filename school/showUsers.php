<?php
session_start();
if (isset($_SESSION['id'])) {
    require_once "dbconnect.php";
    require_once "dbconfig.php";
    require_once "users.php";
    require_once "menu.php";

    $users = new users($dbConnection);
    $allUsers=$users->getAllUsers();
    $users->viewAllUsers($allUsers);

} else {
    $_SESSION['message'] = "You are not logged.";
}

if (isset($_SESSION['message'])) {
    echo "<div id='error_msg'>" . $_SESSION['message'] . "</div>";
    unset($_SESSION['message']);
}

?>

<html>
<body>
<br><button class="button"><a href="addNewUser.php">Add New User</a></button>
<button class="button"><a href="admin.php">Main Menu</a></button>
<br><br>
</body>
</html>

<form method="post" action="logout.php">
    <input type="submit" name="logout" value="Logout">
</form>
