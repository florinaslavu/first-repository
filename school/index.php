<?php
session_start();
require_once "dbconnect.php";
require_once "dbconfig.php";
require_once "users.php";

if ( isset( $_SESSION['id'] ) ) {
    if ($_SESSION['role'] == 0) {
        header('Location: admin.php');
    }
    if ($_SESSION['role'] == 1) {
        header('Location: professor.php');
    }
    if ($_SESSION['role'] == 2) {
        header('Location: pupil.php');
    }
}
    else $_SESSION['message']="You are not logged.";
?>
<?php
if (isset($_SESSION['message'])) {
    echo "<div id='error_msg'>" . $_SESSION['message'] . "</div>";
    unset($_SESSION['message']);
}

?>
<html>
<body>
<button class="button"><a href="login.php">Main Menu</a></button>
<br>
</body>
</html>
